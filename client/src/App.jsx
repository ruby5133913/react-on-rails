import './App.css'
import PostsList from "./features/posts/PostsList"
function App() {
  return(
      <>
      <div className="app">
        <h1>React on Rails</h1>
          <p>find this application in client/src/App.jsx</p>
          <PostsList />
      </div>
      </>
  )
}

export default App
